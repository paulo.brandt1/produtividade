#!/usr/bin/env python

from core.interface import InterfaceDocument
from typing import Type


class Extract:

    def __init__(self):
        self.extract_list = list()

    def extrair_dados_csv(self, model_document: type(InterfaceDocument)):
        instance_document = model_document()
        print('Abrindo arquivos csv...')
        try:
            file = open(instance_document.path, encoding='utf-8')
            cabecalho = file.readline()
            column_name_list = cabecalho.split('\n')[0].split('|')
            print('Arquivo csv aberto com sucesso!')
            self.populate_dict(file.readlines(), column_name_list)
            file.close()
            file_write = open(instance_document.path, 'w')
            file_write.write(cabecalho)
            file_write.close()
        except Exception as e:
            print('Extração não realizada. Erro:{erro}'.format(erro=e))

    def get_data_base(self, clausula_where, document_type: Type[InterfaceDocument]):
        document_object = document_type()
        self.extract_list = document_object.select(clausula_where)

    def populate_dict(self, lista_dados, column_name_list):
        if lista_dados:
            for linha in lista_dados:
                extract_dict = dict()
                raw_data = linha.split("\n")[0].split(";")
                for column, value in zip(column_name_list, raw_data):
                    extract_dict[column] = value
                self.extract_list.append(extract_dict)
            print('Arquivo extraído com sucesso!')
        else:
            print('Não foram encontrados dados para Livros no arquivo.')

