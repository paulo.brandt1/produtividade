#!/usr/bin/env python

from core.models import *
from utils.extract import Extract
from core.interface import InterfaceDocument
from typing import Type


class Commands:
    def __init__(self, comando):
        self.command = comando

    def extract_commands(self):
        while self.command != 'sair':
            print('Que tipo de dados deseja inserir?')
            self.extract_text_comands()
            self.command = input()
            self.extract_control()
            if self.command == '0':
                break

    def search_commands(self):
        while self.command != 'sair':
            print('Que tipo de dados deseja buscar?')
            self.search_text_comands()
            self.command = input()
            self.search_control()
            if self.command == '0':
                break

    def book_commands(self):
        while self.command != 'sair':
            print('Como deseja procurar o livro?')
            self.book_text_comands()
            self.command = input()
            self.book_search_control()
            if self.command == '0':
                break

    def extract_text_comands(self):
        secondary_text = '''
            1 -> Para inserir flask_produtividade
            2 -> Para inserir catalogo de livros
            3 -> Para inserir flask_produtividade de leitura
            4 -> Para inserir dados de artigos científicos
            5 -> Para inserir dados de leitura do artigo
            6 -> Para inserir nota do artigo
            7 -> Para inserir nota dos livros
            8 -> Para inserir flask_produtividade de trabalho
            9 -> Para inserir flask_produtividade de estudo
            ou Digite sair para finalizar
        '''
        print(secondary_text)

    def search_text_comands(self):
        secondary_text = '''
            0 -> Voltar
            1 -> Livro
            ou Digite sair para finalizar
        '''
        print(secondary_text)

    def book_text_comands(self):
        secondary_text = '''
            0 -> Voltar
            1 -> Para buscar dados do livro pelo isbn
            2 -> Para buscar dados do livro pelo título
            3 -> Para buscar dados do livro pela autoria
            4 -> Para buscar dados do livro pelo título original
            5 -> Para buscar dados do livro pelo número de páginas
            6 -> Para buscar dados do livro pela edição
            7 -> Para buscar dados do livro pelo ano de publicação
            8 -> Para buscar dados do livro pela editora
            9 -> Buscar Todos os Livros
            ou Digite sair para finalizar
        '''
        print(secondary_text)

    def extract_control(self):
        model_document = None
        if self.command == '1':
            model_document = DadoProdutividade
        if self.command == '2':
            model_document = Livros
        if self.command == '3':
            model_document = DadosLeitura
        if self.command == '4':
            model_document = ArtigosCientificos
        if self.command == '5':
            model_document = LeituraDoArtigo
        if self.command == '6':
            model_document = ArticleGrades
        if self.command == '7':
            model_document = BookGrades
        if self.command == '8':
            model_document = WorkProductivity
        if self.command == '9':
            model_document = StudyProductivity
        if self.command == 'sair':
            return
        if model_document:
            self.inserir_dados_csv(model_document)

    def main_control(self):
        if self.command == '1':
            self.extract_commands()
        if self.command == '2':
            self.search_commands()
        if self.command == '3':
            self.help()
        if self.command == 'sair':
            return

    def search_control(self):
        if self.command == '1':
            self.book_commands()
        if self.command == 'sair':
            return

    def book_search_control(self, searched_value):
        clausula_where = ''
        if self.command == 'id_isbn':
            clausula_where = 'WHERE id_isbn like(\'{isbn}%\')'.format(isbn=searched_value)
        if self.command == 'titulo':
            clausula_where = 'WHERE titulo like(\'{titulo}%\')'.format(titulo=searched_value)
        if self.command == 'titulo_original':
            clausula_where = 'WHERE titulo_original like(\'{titulo_original}%\')'.format(
                titulo_original=searched_value)
        if self.command == 'autoria':
            clausula_where = 'WHERE autoria like(\'{autoria}%\')'.format(autoria=searched_value)
        if self.command == 'numero_de_paginas':
            clausula_where = 'WHERE numero_de_paginas <= {numero_de_paginas}'.format(numero_de_paginas=searched_value)
        if self.command == 'edicao':
            clausula_where = 'WHERE edicao like(\'{edicao}%\')'.format(edicao=searched_value)
        if self.command == 'ano_de_publicacao':
            clausula_where = 'WHERE ano_de_publicacao = {ano_de_publicacao} '.format(
                ano_de_publicacao=int(searched_value))
        if self.command == 'editora':
            clausula_where = 'WHERE editora like(\'{editora}\'%)'.format(editora=searched_value)
        if self.command == 'sair':
            return
        return self.search_on_database(clausula_where, Livros)

    def help(self):
        text = '''
            1 -> Inserir
            2 -> Buscar
            3 -> Para pedir ajuda e conhecer os comandos disponíveis
                 '''
        print(text)

    def inserir_dados_csv(self, model_document: type(InterfaceDocument)):
        extract_object = Extract()
        extract_object.extrair_dados_csv(model_document)
        for document_data in extract_object.extract_list:
            document_object = model_document()
            document_object.insert_data(document_data)
            document_object.save()

    def search_on_database(self, clausula_where, model_document: Type[InterfaceDocument]):
        extract = Extract()
        extract.get_data_base(clausula_where, model_document)
        response = ''
        for found_date in extract.extract_list:
            instance_model = model_document()
            instance_model.insert_data(found_date)
            response += '<p>' + instance_model.to_string()

        if not extract.extract_list:
            return 'Não foi encontrado nada!'
        return response

