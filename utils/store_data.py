#!/usr/bin/env python

from mysql import connector


class StoreData:

    def __init__(self):
        self.connection = None
        self.cursor = None
        self.open_connection()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()

    def __enter__(self):
        return self

    def open_connection(self):
        self.connection = connector.connect(host="localhost",
                                            user="brandt",
                                            password="ohgma1980",
                                            port=3306,
                                            database="escrita",
                                            charset='utf8mb4')
        self.cursor = self.connection.cursor(dictionary=True)

    def close_connection(self):
        self.connection.close()

    def insert_dados_produtividade(self, dados_de_produtividade):
        sql = '''
        INSERT INTO flask_produtividade 
        (dia,
        tempo,
        numero_de_palavras,
        meta_diaria) 
        VALUES (\'{dia}\',\'{tempo}\',{palavras},{meta});
        '''.format(dia=dados_de_produtividade.dia,
                   tempo=dados_de_produtividade.tempo,
                   palavras=dados_de_produtividade.numero_de_palavras,
                   meta=dados_de_produtividade.meta_diaria)
        self.cursor.execute(sql)
        self.connection.commit()

    def select_dados_de_produtividade(self):
        sql = '''
                SELECT * FROM flask_produtividade;
        '''
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def insert_dados_leitura(self, dados_leitura):
        sql = '''
        INSERT INTO leitura 
        (id_do_livro,
        dia,
        tempo_de_leitura,
        paginas_lidas,
        meta_do_dia, 
        terminei,
        foi_resenhado_no_blog) 
        
        VALUES (\'{id_do_livro}\',
        \'{dia}\',
        \'{tempo_de_leitura}\',
        {paginas_lidas},
        {meta_do_dia},
        {terminei},
        {foi_resenhado_no_blog});
         '''.format(id_do_livro=dados_leitura.id_do_livro,
                    dia=dados_leitura.dia,
                    tempo_de_leitura=dados_leitura.tempo_de_leitura,
                    paginas_lidas=dados_leitura.paginas_lidas,
                    meta_do_dia=dados_leitura.meta_do_dia,
                    terminei=dados_leitura.terminei,
                    foi_resenhado_no_blog=dados_leitura.foi_resenhado_no_blog)
        self.cursor.execute(sql)
        self.connection.commit()

    def select_dados_leitura(self):
        sql = '''
                SELECT * FROM leitura;
                 '''
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def insert_catalogo_livros(self, livros):
        sql = """
            INSERT INTO livros
            (id_isbn, 
            titulo, 
            titulo_original,
            autoria,
            numero_de_paginas,
            genero_literario,
            edicao,
            ano_de_publicacao,
            editora)
    
            VALUES(\"{id_isbn}\",
            \"{titulo}\",
            \"{titulo_original}\",
            \"{autoria}\",
             {numero_de_paginas},
             \"{genero_literario}\",
             \"{edicao}\",
             {ano_de_publicacao},
             \"{editora}\")
            """.format(id_isbn=livros.id_isbn,
                       titulo=livros.titulo,
                       titulo_original=livros.titulo_original,
                       autoria=livros.autoria,
                       numero_de_paginas=livros.numero_de_paginas,
                       genero_literario=livros.genero_literario,
                       edicao=livros.edicao,
                       ano_de_publicacao=livros.ano_de_publicacao,
                       editora=livros.editora)
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except Exception as e:
            print(e)

    def select(self,table, clause_where):
        sql = '''
                SELECT * FROM {table} {where}
                   '''.format(table=table,
                              where=clause_where)
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def select_livros(self, clausula_where):
        sql = '''
                SELECT * FROM livros {where}
                   '''.format(where=clausula_where)
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def insert_artigos_cientificos(self, artigos_cientificos):
        sql = '''
        INSERT INTO artigos_cientificos 
        (id_do_artigo,
        titulo_do_artigo,
        autoria,
        area_de_conhecimento,
        numero_de_paginas_do_artigo,
        impresso_digital,
        ano_de_publicacao)
         
        VALUES (\'{id_do_artigo}\',
        \'{titulo_do_artigo}\',
        \'{autoria}\',
        \'{area_de_conhecimento}\',
        {numero_de_paginas_do_artigo},
        {impresso_digital},
        {ano_de_publicacao})
        '''.format(id_do_artigo=artigos_cientificos.id_do_artigo,
                   titulo_do_artigo=artigos_cientificos.titulo_do_artigo,
                   autoria=artigos_cientificos.autoria,
                   area_de_conhecimento=artigos_cientificos.area_de_conhecimento,
                   numero_de_paginas_do_artigo=artigos_cientificos.numero_de_paginas_do_artigo,
                   impresso_digital=artigos_cientificos.impresso_digital,
                   ano_de_publicacao=artigos_cientificos.ano_de_publicacao)
        self.cursor.execute(sql)
        self.connection.commit()

    def insert_leitura_do_artigo(self, leitura_do_artigo):
        sql = """
                   INSERT INTO leitura_do_artigo
                   (id_do_artigo, 
                   paginas_lidas, 
                   terminei_em,
                   fiz_resumo,
                   resumo_docs)
    
                   VALUES(\'{id_do_artigo}\',
                   {paginas_lidas},
                   \'{terminei_em}\',
                   {fiz_resumo},
                   {resumo_docs})
                   """.format(id_do_artigo=leitura_do_artigo.id_do_artigo,
                              paginas_lidas=leitura_do_artigo.paginas_lidas,
                              terminei_em=leitura_do_artigo.terminei_em,
                              fiz_resumo=leitura_do_artigo.fiz_resumo,
                              resumo_docs=leitura_do_artigo.resumo_docs)
        self.cursor.execute(sql)
        self.connection.commit()

    def select_leitura_do_artigo(self):
        sql = '''
                 SELECT * FROM leitura_do_artigo
         '''
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def select_article_grades(self, clausule):
        sql = '''
                   SELECT * FROM article_grades;
           '''
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def insert_article_grades(self, article_grades):
        sql = """
                      INSERT INTO article_grades
                      (article_id,
                      article_grades)

                      VALUES
                      (\"{article_id}\",
                      {article_grades})
                      """.format(id=article_grades.id,
                                 article_id=article_grades.article_id,
                                 article_grades=article_grades.grades)
        self.cursor.execute(sql)
        self.connection.commit()

    def select_book_grades(self):
        sql = '''
                   SELECT * FROM book_grades;
           '''
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def insert_book_grades(self, book_grades):
        sql = """
                      INSERT INTO book_grades
                      (book_id,
                      book_grades)

                      VALUES
                      (\"{book_id}\",
                      {book_grades})
                      """.format(id=book_grades.id,
                                 book_id=book_grades.id,
                                 book_grades=book_grades.grades)
        self.cursor.execute(sql)
        self.connection.commit()

    def select_work_productivity(self):
        sql = '''
                     SELECT * FROM work_productivity;
             '''
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def insert_work_productivity(self, work_productivity):
        sql = '''        
                    INSERT INTO work_productivity 
                    (day,
                    focus_time,
                    theory_practice,
                    knowledge_area) 
                    
                    VALUES (\'{day}\',
                    \'{focus_time}\',
                    {theory_practice},
                    \'{knowledge_area}\')
                     '''.format(day=work_productivity.day,
                                focus_time=work_productivity.focus_time,
                                theory_practice=work_productivity.theory_practice,
                                knowledge_area=work_productivity.knowledge_area)

        self.cursor.execute(sql)
        self.connection.commit()

    def select_study_productivity(self):
        sql = '''
                      SELECT * FROM study_productivity;
              '''
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def insert_study_productivity(self, study_productivity):
        sql = """
                         INSERT INTO study_productivity
                         (knowledge_area,
                         day,
                         kind_of_study,
                         time_of_study,
                         course,
                         finished,
                         course_diploma)

                         VALUES
                         (\"{knowledge_area}\",
                         \'{day}\',
                         \'{kind_of_study}\',
                         \'{time_of_study}\',
                         {course},
                         {finished},
                         {course_diploma})
                         """.format(knowledge_area=study_productivity.knowledge_area,
                                    day=study_productivity.day,
                                    kind_of_study=study_productivity.kind_of_study,
                                    time_of_study=study_productivity.time_of_study,
                                    course=study_productivity.course,
                                    finished=study_productivity.finished,
                                    course_diploma=study_productivity.course_diploma)
        self.cursor.execute(sql)
        self.connection.commit()

    def _select_livros(self, clausula_where):
        sql = '''
                SELECT * FROM livros \'{where}\')

        '''.format(where=clausula_where)
        self.cursor.execute(sql)
        return self.cursor.fetchall()