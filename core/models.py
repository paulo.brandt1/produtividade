#!/usr/bin/env python

from utils.store_data import StoreData
from core.interface import InterfaceDocument


class DadoProdutividade(InterfaceDocument):
    def __init__(self):
        self.path = 'arquivos_csv/produtividade.csv'
        self.table = 'flask_produtividade'
        self.json_data = None
        self.dia = None
        self.tempo = None
        self.numero_de_palavras = None
        self.meta_diaria = None

    def insert_data(self, productivity_data) -> None:
        self.dia = productivity_data['dia']
        self.tempo = productivity_data['tempo']
        self.numero_de_palavras = productivity_data['numero_de_palavras']
        self.meta_diaria = productivity_data['meta_diaria']
        self.json_data = productivity_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_dados_produtividade(self)
            print('Dados salvos com sucesso.')


class Livros(InterfaceDocument):
    def __init__(self):
        self.table = 'livros'
        self.path = 'arquivos_csv/livros.csv'
        self.json_data = None
        self.id_isbn = None
        self.titulo = None
        self.titulo_original = None
        self.autoria = None
        self.numero_de_paginas = None
        self.genero_literario = None
        self.edicao = None
        self.ano_de_publicacao = None
        self.editora = None

    def insert_data(self, book_data):
        self.id_isbn = book_data['id_isbn']
        self.titulo = book_data['titulo']
        self.titulo_original = book_data['titulo_original']
        self.autoria = book_data['autoria']
        self.numero_de_paginas = book_data['numero_de_paginas']
        self.genero_literario = book_data['genero_literario']
        self.edicao = book_data['edicao']
        self.ano_de_publicacao = book_data['ano_de_publicacao']
        self.editora = book_data['editora']
        self.json_data = book_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_catalogo_livros(self)
            print('Dados salvos com sucesso.')

    def get_titulo(self):
        return self.titulo

    def get_id_isbn(self):
        return self.id_isbn

    def get_autoria(self):
        return self.autoria

    def get_titulo_original(self):
        return self.titulo_original

    def get_numero_de_paginas(self):
        return self.numero_de_paginas

    def get_edicao(self):
        return self.edicao

    def get_ano_de_publicacao(self):
        return self.ano_de_publicacao

    def get_editora(self):
        return self.editora


class ArticleGrades(InterfaceDocument):
    def __init__(self):
        self.path = 'arquivos_csv/article_grades.csv'
        self.table = 'article_grades'
        self.json_data = None
        self.id = None
        self.article_id = None
        self.article_grades = None

    def insert_data(self, article_grades_data) -> None:
        self.id = article_grades_data['id']
        self.article_id = article_grades_data['article_id']
        self.article_grades = article_grades_data['article_grades']
        self.json_data = article_grades_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_article_grades(self)
            print('Dados salvos com sucesso.')



class ArtigosCientificos(InterfaceDocument):
    def __init__(self):
        self.table = 'artigos_cientificos'
        self.path = 'arquivos_csv/artigos_cientificos.csv'
        self.json_data = None
        self.id_do_artigo = None
        self.titulo_do_artigo = None
        self.autoria = None
        self.area_de_conhecimento = None
        self.numero_de_paginas_do_artigo = None
        self.impresso_digital = None
        self.ano_de_publicacao = None

    def insert_data(self, scientific_article_data) -> None:
        self.id_do_artigo = scientific_article_data['id_do_artigo']
        self.titulo_do_artigo = scientific_article_data['titulo_do_artigo']
        self.autoria = scientific_article_data['autoria']
        self.area_de_conhecimento = scientific_article_data['area_de_conhecimento']
        self.numero_de_paginas_do_artigo = scientific_article_data['numero_de_paginas_do_artigo']
        self.impresso_digital = scientific_article_data['impresso_digital']
        self.ano_de_publicacao = scientific_article_data['ano_de_publicacao']
        self.json_data = scientific_article_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_artigos_cientificos(self)
            print('Dados salvos com sucesso.')


class BookGrades(InterfaceDocument):
    def __init__(self):
        self.path = 'arquivos_csv/book_grades.csv'
        self.table = 'book_grades'
        self.json_data = None
        self.id = None
        self.book_id = None
        self.book_grades = None

    def insert_data(self, book_grades_data) -> None:
        self.id = book_grades_data['id']
        self.book_id = book_grades_data['book_id']
        self.book_grades = book_grades_data['book_grades']
        self.json_data = book_grades_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_book_grades(self)
            print('Dados salvos com sucesso.')


class LeituraDoArtigo(InterfaceDocument):
    def __init__(self):
        self.table = 'leitura_do_artigo'
        self.json_data = None
        self.path = 'arquivos_csv/leitura_do_artigo.csv'
        self.id_do_artigo = None
        self.paginas_lidas = None
        self.terminei_em = None
        self.fiz_resumo = None
        self.resumo_docs = None

    def insert_data(self, article_read_data):
        self.id_do_artigo = article_read_data['id_do_artigo']
        self.paginas_lidas = article_read_data['paginas_lidas']
        self.terminei_em = article_read_data['terminei_em']
        self.fiz_resumo = article_read_data['fiz_resumo']
        self.resumo_docs = article_read_data['resumo_docs']
        self.json_data = article_read_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_leitura_do_artigo(self)
            print('Dados salvos com sucesso.')


class DadosLeitura(InterfaceDocument):
    def __init__(self):
        self.table = 'leitura'
        self.json_data = None
        self.path = 'arquivos_csv/leitura.csv'
        self.id_do_livro = None
        self.dia = None
        self.tempo_de_leitura = None
        self.paginas_lidas = None
        self.meta_do_dia = None
        self.terminei = None
        self.foi_resenhado_no_blog = None

    def insert_data(self, read_data):
        self.id_do_livro = read_data['id_do_livro']
        self.dia = read_data['dia']
        self.tempo_de_leitura = read_data['tempo_de_leitura']
        self.paginas_lidas = read_data['paginas_lidas']
        self.meta_do_dia = read_data['meta_do_dia']
        self.terminei = read_data['terminei']
        self.foi_resenhado_no_blog = read_data['foi_resenhado_no_blog']
        self.json_data = read_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_dados_leitura(self)
            print('Dados salvos com sucesso.')


class StudyProductivity(InterfaceDocument):
    def __init__(self):
        self.table = 'study_productivity'
        self.json_data = None
        self.path = 'arquivos_csv/study_productivity.csv'
        self.knowledge_area = None
        self.day = None
        self.kind_of_study = None
        self.time_of_study = None
        self.course = None
        self.finished = None
        self.course_diploma = None

    def insert_data(self, study_productivity_data):
        self.knowledge_area = study_productivity_data['knowledge_area']
        self.day = study_productivity_data['day']
        self.kind_of_study = study_productivity_data['kind_of_study']
        self.time_of_study = study_productivity_data['time_of_study']
        self.course = study_productivity_data['course']
        self.finished = study_productivity_data['finished']
        self.course_diploma = study_productivity_data['course_diploma']
        self.json_data = study_productivity_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_study_productivity(self)
            print('Dados salvos com sucesso.')


class WorkProductivity(InterfaceDocument):
    def __init__(self):
        self.table = 'work_productivity'
        self.json_data = None
        self.path = 'arquivos_csv/work_productivity.csv'
        self.day = None
        self.focus_time = None
        self.theory_practice = None
        self.knowledge_area = None

    def insert_data(self, work_productivity_data):
        self.day = work_productivity_data['day']
        self.focus_time = work_productivity_data['focus_time']
        self.theory_practice = work_productivity_data['theory_practice']
        self.knowledge_area = work_productivity_data['knowledge_area']
        self.json_data = work_productivity_data

    def save(self):
        with StoreData() as connection:
            print('Salvando dados...')
            connection.insert_work_productivity(self)
            print('Dados salvos com sucesso.')
