from abc import ABC, abstractmethod
from utils.store_data import StoreData


class InterfaceDocument(ABC):

    @abstractmethod
    def __init__(self):
        self.table = ''
        self.json_data = None
        self.path = ''

    @abstractmethod
    def insert_data(self, data) -> None:
        pass

    @abstractmethod
    def save(self) -> None:
        pass

    def select(self, where_clause) -> tuple:
        with StoreData() as connection:
            return connection.select(self.table, where_clause)

    def to_string(self) -> str:
        to_print = ''
        for column, value in self.json_data.items():
            to_print += '<p>' + column.replace('_', ' ').capitalize() + ': ' + str(value)
        return to_print

    def get_insert_template(self):
        file = open(self.path)
        head_template_list = file.readline().split('\n')[0].split('|')
        width_template_list = file.readline().split('\n')[0].split('|')
        file.close()
        template_html_string = ''
        for column, width in zip(head_template_list, width_template_list):
            template_html_string += f'''
            <div class="Model">
                <p>{column.replace('_', ' ').capitalize()}</p>
                <div class="" style="width:{width}px;">
                    <input type="text" name="{column}" placeholder=""/>
                </div>
            </div>
            '''
        marcador = f'''
                    <div class="Model">
                        <input name="model" type="text" value="{self.table}"/>
                    </div>
                    '''
        return template_html_string + marcador
