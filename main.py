#!/usr/bin/env python
from flask import Flask, render_template, request
from utils.commands import Commands
from core.interface import InterfaceDocument
from core.models import *
from typing import Type

app = Flask(__name__)
model_document = None

def get_instance_document(model)-> InterfaceDocument:
    if model == 'livros':
        return Livros()
    if model == 'article_grades':
        return ArticleGrades()
    if model == 'artigos_cientificos':
        return ArtigosCientificos()
    if model == 'book_grade':
        return BookGrades()
    if model == 'leitura':
        return DadosLeitura()
    if model == 'leitura_do_artigo':
        return LeituraDoArtigo()
    if model == 'produtividade':
        return DadoProdutividade()
    if model == 'study_productivity':
        return StudyProductivity()
    if model == 'work_productivity':
        return WorkProductivity()


@app.route('/', methods=['GET', 'POST'])
def inicial():
    return render_template('inicial.html')


@app.route('/insert', methods=['GET', 'POST'])
def insert():
    if request.method == 'POST':
        if request.form.get('choose'):
            model_document = get_instance_document(request.form.get('choose'))
            request.method = ''
            file = open('templates/insert_top.html')
            top_html = file.read()
            file.close()
            file = open('templates/insert_botton.html')
            botton_html = file.read()
            template_html = model_document.get_insert_template()
            return top_html + template_html + botton_html
        else:
            model_document = get_instance_document(request.form.get('model'))
            model_document.insert_data(request.form.to_dict())
            model_document.save()

    return render_template('decision_insert.html')


def result(result_string):
    return result_string

@app.route('/search_book', methods=['GET', 'POST'])
def search_book():
    if request.method == 'POST':
        campo = request.form.get('campo')
        busca = request.form.get('busca')
        return result(Commands(campo).book_search_control(busca))
    return render_template('search_book.html')

# def main(commands):
#     commands.main_control()
#     while commands.command != 'sair':
#         print('Escreva no prompt os comandos desejados:')
#         commands.command = input()
#         commands.main_control()


if __name__ == '__main__':
    app.run(debug=True)


